#include "ADBInterface.hpp"

#include <boost/tokenizer.hpp>
#include <algorithm>
#include <stdexcept>
#include <cstdlib>

#include <iostream>

ADBInterface::ADBInterface()
{
}

ADBInterface::ADBInterface(const std::string& adb_binary)
{
    bin_ = adb_binary;
}

#ifndef ADBSCREENCAPTURE_NO_LOG
#   define ADBSCREENCAPTURE_LOG(x) (std::cerr << ("LOG: " x "\n"))
#else
#   define ADBSCREENCAPTURE_LOG(x) ((void) 0)
#endif

bool readBytesEOLConv(std::istream &is, char *buf, std::size_t n)
{
    std::size_t read = 0;
    bool cr = false;
    char cur = '\0';
    while (read < n)
    {
        if (!is.read(&cur, 1)) break;
        if (cr)
        {
            if (cur == '\n')
            {
                buf[read] = '\n';
                read++;
            }
            else
            {
                buf[read] = '\r';
                read++;
                if (read < n)
                {
                    buf[read] = cur;
                    read++;
                }
            }
            cr = false;
        }
        else
        {
            if (cur == '\r')
            {
                cr = true;
            }
            else
            {
                buf[read] = cur;
                read++;
            }
        }
    }
    return read == n;
}

std::vector<std::string> tokenize(const std::string& input)
{
    typedef boost::escaped_list_separator<char> separator_type;
    separator_type separator("\\",    // The escape characters.
                             "= ",    // The separator characters.
                             "\"\'"); // The quote characters.

    // Tokenize the intput.
    boost::tokenizer<separator_type> tokens(input, separator);

    // Copy non-empty tokens from the tokenizer into the result.
    std::vector<std::string> result;
    std::copy_if(tokens.begin(), tokens.end(), std::back_inserter(result),
    [](const std::string &in) {
        return !in.empty();
    });
    return result;
}

namespace bp = boost::process;

void ADBInterface::SetBin(const std::string& adb_binary)
{
    bin_ = adb_binary;
}

std::string ADBInterface::GetBin() const
{
    return bin_;
}

void ADBInterface::Launch(const std::string& adb_args)
{
    args_ = tokenize("adb " + adb_args);

    bp::context ctx;
    ctx.environment = bp::self::get_environment();
    ctx.stderr_behavior = bp::capture_stream();
    ctx.stdout_behavior = bp::capture_stream();
    ctx.stdin_behavior = bp::capture_stream();

    child_ = bp::launch(bin_, args_, ctx);
}

int ADBInterface::Close()
{
    child_->get_stdin() << "exit" << std::endl;
    return child_->wait().exit_status();
}

wxImage ADBInterface::CaptureImage(const std::string &screencap_cmd)
{
    if (child_)
    {
        auto &pis = child_->get_stdout();
        auto &pis_error = child_->get_stderr();
        auto &pos = child_->get_stdin();
        std::string dummy_line;
        if (std::getline(pis, dummy_line, '$'))
        {
            ADBSCREENCAPTURE_LOG("Seems to be connected.");

            pos << screencap_cmd << std::endl; // send screencap cmd

            std::size_t ignore_n = screencap_cmd.size() + 4;
                /* <space> screencap \r\r\n */

            pis.ignore(ignore_n);

            std::uint32_t wi, he, format;

            readBytesEOLConv(pis, (char *) &wi, 4);
            readBytesEOLConv(pis, (char *) &he, 4);
            readBytesEOLConv(pis, (char *) &format, 4);

            if (format == 1)
            {
                char *rgb_data = (char *) std::malloc(wi * he * 3);
                char *alpha_data = (char *) std::malloc(wi * he * 1);

#if 0   // this causes latency, during which, USB buffer overflows. reading at once is a better idea.
                char *rgb_data_cur = rgb_data;
                char *alpha_data_cur = alpha_data;

                ADBSCREENCAPTURE_LOG("Begin transmission.");

                std::size_t total = wi * he;
                for (std::size_t i = 0; i < total; ++i)
                {
                    readBytesEOLConv(pis, rgb_data_cur, 3);
                    readBytesEOLConv(pis, alpha_data_cur, 1);

                    std::cout.write(rgb_data_cur, 4);

                    rgb_data_cur += 3;
                    alpha_data_cur += 1;
                }

                ADBSCREENCAPTURE_LOG("End transmission.");
#else
                std::vector<char> data;
                data.resize(wi * he * 4);
                readBytesEOLConv(pis, &data[0], wi * he * 4);

                std::size_t total = wi * he;
                for (std::size_t i = 0; i < total; ++i)
                {
                    std::memcpy((rgb_data + 3 * i), &data[0] + 4 * i, 3);
                    std::memcpy((alpha_data + i), &data[0] + 4 * i + 3, 1);
                }
#endif

                return wxImage(
                    wi,
                    he,
                    (unsigned char *) rgb_data,
                    (unsigned char *) alpha_data,
                    false /* transfer ownerships */);
            }
            else
            {
                // TODO support other file formats
                throw std::runtime_error("Unimplemented image format!");
            }
        }
        else
        {
            throw std::runtime_error("Unexpected end of the stream!");
        }
    }
    else
    {
        throw std::runtime_error("Not launched process!");
    }
}

ADBInterface::~ADBInterface()
{
    if (child_.is_initialized()) Close();
}
