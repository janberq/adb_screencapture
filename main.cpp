#include <wx/app.h>
#include <wx/frame.h>
#include <wx/statbmp.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/menu.h>

#include <iostream>

#include "ADBInterface.hpp"

class App : public wxApp
{
public:
    bool OnInit() override;
};

wxDECLARE_APP(App);

class MainFrame : public wxFrame
{

public:

    MainFrame();
    MainFrame(wxWindow *parent, wxWindowID id);
    bool Create(wxWindow *parent, wxWindowID id);
    virtual ~MainFrame();

    void Reload();

private:

    wxPanel *root_;
    wxStaticBitmap *view_;

    ADBInterface adb_;

};

//
//  IMPL
//

bool App::OnInit()
{
    MainFrame *frm = new MainFrame(nullptr, wxID_ANY);
    frm->Show(true);
    return true;
}

wxIMPLEMENT_APP(App);

MainFrame::MainFrame()
{
    // nothing
}

MainFrame::MainFrame(wxWindow* parent, wxWindowID id)
{
    MainFrame::Create(parent, id);
}

bool MainFrame::Create(wxWindow* parent, wxWindowID id)
{
    if (!wxFrame::Create(parent, id, "ADB Test"))
        return false;

    // first, create the menu
    {
        wxMenuBar *menubar = new wxMenuBar;

        wxMenu *file = new wxMenu;
        file->Append(wxID_REFRESH, _("Refresh\tF5"));
        file->AppendSeparator();
        file->Append(wxID_EXIT, _("Exit\tALT+F4"));

        menubar->Append(file, _("File"));

        menubar->Bind(wxEVT_MENU, [this](wxCommandEvent &evt) {
            Reload();
        }, wxID_REFRESH);
        menubar->Bind(wxEVT_MENU, [this](wxCommandEvent &evt) {
            Close();
        }, wxID_EXIT);

        SetMenuBar(menubar);
    }

    // create the root panel
    root_ = new wxPanel(this, wxID_ANY);

    // create the static bmp
    view_ = new wxStaticBitmap(root_, wxID_ANY, wxNullBitmap);

    // create sizers
    wxBoxSizer *root_sizer = new wxBoxSizer(wxVERTICAL);
    root_sizer->Add(root_, wxSizerFlags(1).Expand().Border(0, wxALL));
    SetSizer(root_sizer);

    wxBoxSizer *sizer1 = new wxBoxSizer(wxVERTICAL);
    sizer1->Add(view_, wxSizerFlags(1).Expand());

    root_->SetSizer(sizer1);

    adb_.SetBin("/home/janberq/Android/Sdk/platform-tools/adb");
    adb_.Launch("shell");

    return true;
}

MainFrame::~MainFrame()
{
}

void MainFrame::Reload()
{
    wxImage img = adb_.CaptureImage();
    view_->SetBitmap(wxBitmap(img));
    view_->Refresh();
}
