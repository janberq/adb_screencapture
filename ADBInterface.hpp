#ifndef _ADBINTERFACE_HPP_INCLUDED
#define _ADBINTERFACE_HPP_INCLUDED

#include <wx/image.h>
#include <vector>
#include <boost/process.hpp>
#include <boost/optional.hpp>

class ADBInterface
{

public:

    ADBInterface();
    ADBInterface(const std::string &adb_binary);

    void SetBin(const std::string &adb_binary);
    std::string GetBin() const;

    void Launch(const std::string &adb_args);

    int Close();

    wxImage CaptureImage(const std::string &screencap_cmd = "screencap");

    virtual ~ADBInterface();

private:

    std::string bin_;
    std::vector<std::string> args_;
    boost::optional<boost::process::child> child_;

};

#endif // _ADBINTERFACE_HPP_INCLUDED
